local socket = require "socket"
local json = require "json"
local DEBUG = false

local DEGREES = 3.14159/180
local TICKTIME = 1/60
local G = 9.81

local need_schooling = false


local turnMessage = "" -- for debugging only

local max_angle = 59.5
local max_recorded_angle = 0

local force_multiplier = 22 -- this is arbitrary but it figures itself out over time
local ticks = 0 -- total time.
local m = 1

local crashes = 0

engine_power = 600

--]]

-- The following constants are only to be used for decceleration with thr = 0
local lazyticks = 0 -- time we've had our foot off the accelerator
local decel_constant = 1.22 -- we're going to calculate this as we go but pick a decent value to start
local deccel_slope = 10.0 -- we're going to calculate this too 

math.sign = function(a) return a == 0 and 0 or (a > 0 and 1 or -1) end

function GetDecelTime(start_speed, end_speed)
  return ((start_speed / end_speed) - 1) / decel_constant
end

function GetDecelDist(start_speed, end_speed)
  -- TODO: THIS a cool little function but it might need some love. Let's see how we can make it better
  return (start_speed/decel_constant)*math.log(start_speed/end_speed)
end

local NoobBot = {}
NoobBot.__index = NoobBot

function NoobBot.create(conn, name, key, track)
   local bot = {}
   setmetatable(bot, NoobBot)
   bot.conn = conn
   bot.name = name
   bot.key = key
   bot.trackname = track
   return bot
end

function NoobBot:msg(msgtype, data)
   return json.encode.encode({msgType = msgtype, data = data, gameTick=self.tick})
end

function NoobBot:send(msg)
   --print("Sending msg: %s", msg)
   self.conn:send(msg .. "\n")
end

function NoobBot:createRace()
  
   local dat = {trackName = self.trackname, botId={name = self.name, key = self.key}, carCount = 3, password = "abc123" }

   self:send(self:msg("createRace", dat))
   print(string.format("Creating Race: %s on track %s\n", self.password or "", self.trackname))
   self:msgloop()

end

function NoobBot:joinTestRace()
  
   local dat = {trackName = self.trackname, botId={name = self.name, key = self.key}, carCount = 3, password = "abc123" }
   self:send(self:msg("joinRace", dat))
   print(string.format("Joining Race: %s on track %s\n", self.password or "", self.trackname))
   self:msgloop()

end

function NoobBot:join()
  
  if self.trackname then
   local dat = {trackName = self.trackname, botId={name = self.name, key = self.key} }
   return self:msg("joinRace", dat)

  else
   local dat = {name = self.name, key = self.key}
   return self:msg("join", dat)
  end
end

function NoobBot:onyourcar(data)
   print(string.format("We are car named %s\n", data.name))
   self.carname = data.name
end


function NoobBot:throttle(throttle)
   return self:msg("throttle", throttle)
end

function NoobBot:turbo()
   return self:msg("turbo", "woooo")
end

function NoobBot:switch(left)
    self.messages = (self.messages or 0) .. "Switch " .. (left and "Left" or "Right")
   return self:msg("switchLane", left and "Left" or "Right")
end

function NoobBot:ping()
   return self:msg("ping", {})
end

function NoobBot:run()
   self:send(self:join())
   print(string.format("Joining track: %s\n", self.trackname or ""))
   self:msgloop()
end

function NoobBot:onjoin(data)
   print("Joined")
   self:send(self:ping())
end

function NoobBot:ongameinit(data)
   print ("GAME INIT")
   self.lanes = {}
   self.track = {}
   self.cars = data.race.cars
   self.maxlane =  0
   self.track_id = data.race.track.id

   self.printInit()

   for k, v in pairs(data.race.track.lanes) do
      self.lanes[v.index] = v.distanceFromCenter
      self.maxlane = math.max(self.maxlane, v.index)
   end

   for k,v in ipairs(data.race.track.pieces) do
      self.track[k-1] = v

      v.id = k
      v.next_track = data.race.track.pieces[k < #data.race.track.pieces and k+1 or 1]
      v.next_track.last_track = v

      -- If we crash or underwhelm we can adjust the curve
      v.force_modifier = 0

      v.arc_length = {}
      if v.length then
         for lane,offset in pairs(self.lanes) do
            v.arc_length[lane] = v.length
         end
      else
         for lane,offset in pairs(self.lanes) do
            v.arc_length[lane] = (v.radius + offset *(v.angle < 0 and 1 or -1)) * math.abs(v.angle)*DEGREES
         end
      end
   end


  self.longest_straightaway = nil
  for k,v in ipairs(data.race.track.pieces) do
    local track = v
    local len = 0
    if track.length then
      while track.next_track.length do
        len = len + track.next_track.length
        track = track.next_track
      end
    end
    v.straightaway_length = len
    self.longest_straightaway = self.longest_straightaway and ( self.longest_straightaway.straightaway_length < v.straightaway_length and v or self.longest_straightaway) or v

  end

end

function NoobBot:ongamestart(data)
   self:send(self:ping())
end

--returns true if we want to signal a lane change
function NoobBot:doLaneSwitch()
   --do return false end
   local dat = self.cardata[self.carname]

   if dat.track.next_track.switch and dat.oldtrack and dat.oldtrack ~= dat.track then
            
      local max_lookahead = 10
      local left = 0
      local right = 0
      local track = dat.track.next_track.next_track
      while max_lookahead > 0 do
         if track.switch then
            break
         end

         if track.angle and track.angle < 0 then
            left = left + 1
         elseif track.angle and track.angle > 0 then
            right = right + 1
         end
         track = track.next_track
         max_lookahead = max_lookahead - 1
      end

      if left > 0 and left > right and dat.lane > 0 then
         self:send(self:switch(true))
         return true--return
      elseif right > 0 and right > left and dat.lane < self.maxlane then
         self:send(self:switch(false))
         return true--return
      end
   end


end

function NoobBot:onturboavailable(data)
  self.turboAvailable = true
  self.turboTime = data.turboDurationMilliseconds / 1000
end

function NoobBot:updateThrottle()
   local dat = self.cardata[self.carname]
   self.thr = 1

  self.controlString = ""

  if dat.track.angle and math.abs(dat.track.angle) >= 20 and dat.oldtrack ~= dat.track then
    self.thr = 0
    self.controlString = "New turn " .. turnMessage
    turnMessage = ""
  end


  if dat.track.angle or dat.lanechange then 
    --*******
    local time_to_curve_end = (dat.lanechange and dat.track.length or dat.dist_to_end_of_curve)/ dat.speed + TICKTIME*4
    
    local max_time = 0
    if math.abs(dat.angle_accel) > 0 then
       max_time = -dat.angle_speed / dat.angle_accel
       if max_time < 0 then max_time = time_to_curve_end end
    end

    local max_drift = dat.angle + dat.angle_speed*max_time + .5*dat.angle_accel*max_time*max_time
    local end_drift = dat.angle + dat.angle_speed*time_to_curve_end + .5*dat.angle_accel*time_to_curve_end*time_to_curve_end
    
    local drift = max_time <= time_to_curve_end + TICKTIME*4 and max_drift or end_drift
    

    -- Basic throttle shaping over a turn. helps smooth out the acceleration profile
    if dat.track.angle and self.thr > 0 then
      local current_curve_progress = (dat.curve_length - dat.dist_to_end_of_curve) / dat.curve_length
      if current_curve_progress < 0.5 then
        self.thr = math.sin(math.pi * current_curve_progress )
      else
        self.thr = 1
      end
    end

    if (dat.lanechange or math.sign(dat.angle_speed) == math.sign(dat.track.angle)) and math.abs(drift) >= max_angle then
       self.controlString = "BrakeTurn"
       self.thr = 0
    end
  end


  -- The following emergency conditions will cause thrust to drop to zero

  if self:ShouldSlowForUpcomingCurve() then
    self.thr = 0
  end

  -- TODO: This needs to be better
  if dat.track.length and math.abs(dat.angle + dat.angle_speed*TICKTIME*4) > 59 then
    self.thr = 0
    self.controlString = "SPECIAL OVERRIDE"
  end

end

function NoobBot:ShouldSlowForUpcomingCurve()

  local dat = self.cardata[self.carname]

  if math.abs(dat.upcoming_turn_radius) <= 0 then return false end

  local target_speed

    -- TODO: Need an energy bank to see if we're going into the curve with a surplus of momentum
    -- centripetal force is the killer at the start of a turn and it varies with Vt^2 / r

    local old_speed  = target_speed

    -- Let's factor in learning ability
    local adjusted_force_multiplier = force_multiplier + (dat.upcoming_turn_force_modifier or 0)

    local target_speed = math.sqrt( dat.upcoming_turn_radius * adjusted_force_multiplier * 100 )
     
    -- DEBUG ONLY
    turnMessage = string.format("\t%0.1f\t%0.1f\t%0.1f", dat.upcoming_turn_force_modifier or 0, adjusted_force_multiplier, target_speed)

    if target_speed and dat.speed > target_speed then
       local dist_req = GetDecelDist(dat.speed, target_speed)
       if dist_req >= dat.dist_to_curve - dat.speed*TICKTIME then
          self.controlString = "PreTurnBrake" 
          return true
       end

  end
end

function NoobBot:printInit()

  if PRINTOUT ~= nil and PRINTOUT == 1 then
    print("track\tSegment\tTime\tTan Speed\tTan Accel\tAngle\tAng. V\tAng. A\tMax Angle\tThrust\tTurbo\tTrack Angle\tTrack Radius\tFinal Angle\td next curve\tControlString\tMessagest")
  end

end


function NoobBot:printStatus()
   local dat = self.cardata[self.carname]

   if PRINTOUT ~= nil and PRINTOUT == 1 then
      local t= #dat.pos*TICKTIME
      local str = (dat.track.length and "S" or (dat.track.angle < 0 and "L" or "R"))..dat.track.id

      if dat.track.switch then str = str .. "s" end
      local turbo = (self.turboAvailable and "*" or "") .. (self.turboON and "T" or "")

      local print_max_angle = ""
      if dat.angle > max_recorded_angle then
        max_recorded_angle = dat.angle
        print_max_angle = string.format("%2.2f",max_recorded_angle)
      end

      -- DEBUGGING
      if dat.angle_accel > 2500 then self.message = " --ANGULAR ACCEL SPIKE-- " .. (self.message or "") end
    
    -- no sense printing crash data
    if math.abs(dat.speed) > 0 or math.abs(dat.accel) > 0 then

      -- This si getting nuts. let's split it up
      local printstring = string.format("%s\t%s\t%d\t", TRACK or "", str, ticks )
      printstring = printstring .. string.format("%.1f\t%0.1f\t%2.2f\t%.1f\t%.1f\t%s\t", dat.speed or 0, dat.accel or 0, dat.angle or 0, dat.angle_speed, dat.angle_accel, print_max_angle)
      printstring = printstring .. string.format("%0.2f\t%s\t%d\t%d\t", self.thr or 0, turbo, dat.track.angle or 0, dat.track.radius or 0)
      printstring = printstring .. string.format("%0.1f\t%s\t%s", dat.dist_to_curve ,self.controlString or "", self.messages or "")

      print(printstring)
      self.messages = ""
    end

      
   end
end


function NoobBot:onturbostart()
  self.turboON = true
  self.turboAvailable = false
end

function NoobBot:onturboend()
  self.turboON = false
end


function NoobBot:goAtSpeed(speed)

  local dat = self.cardata[self.carname]

  local err = speed - dat.speed
  local old_err = speed - (dat.oldspeed or 0)
  local err_speed = (err - old_err)

  local p = .01
  local d = .03

  self.thr = math.max(math.min(1, (self.thr or 0) + err*p + err_speed*d),0)
end


function NoobBot:oncarpositions(data)
  
   self.tick = data.gameTick
   self.cardata = self.cardata or {}

   ticks = ticks + 1

   for k,v in pairs(data) do
      self.cardata[v.id.name] = self.cardata[v.id.name] or {}
      local dat = self.cardata[v.id.name]
      if v.id.name == self.carname then

        do
           --collect info about positions
           dat.oldtrack = dat.track
           dat.olddist = dat.dist or 0
           dat.track = self.track[v.piecePosition.pieceIndex]
           dat.dist = v.piecePosition.inPieceDistance
           dat.lap = lap
           dat.old_angle = dat.angle or 0
           dat.angle = v.angle

           dat.old_angle_speed = dat.angle_speed or 0
           dat.angle_speed = (dat.angle - dat.old_angle)/TICKTIME or 0
           dat.angle_accel = (dat.angle_speed - dat.old_angle_speed)/TICKTIME
           dat.lane = (dat.dist < .5*(dat.track.angle and dat.track.radius*dat.track.angle*DEGREES or dat.track.length)) and v.piecePosition.lane.startLaneIndex or v.piecePosition.lane.endLaneIndex
           dat.oldlanechange = dat.lanechange
           dat.lanechange = v.piecePosition.lane.startLaneIndex ~= v.piecePosition.lane.endLaneIndex
           dat.last_lane_start = dat.lane_start or dat.lane
           dat.last_lane_end = dat.lane_end or dat.lane
           dat.lane_start = v.piecePosition.lane.startLaneIndex 
           dat.lane_end = v.piecePosition.lane.endLaneIndex

           --accumulate positions for analysis
           dat.pos = dat.pos or {}
           dat.totaldist = dat.totaldist or 0
           local delta = 0
           if not dat.oldtrack or dat.oldtrack == dat.track then
              delta = dat.dist - (dat.olddist or 0)
           else


              if dat.oldlanechange then
                 if dat.oldtrack.angle then
                    local arclen = (dat.oldtrack.arc_length[v.piecePosition.lane.startLaneIndex] + dat.oldtrack.arc_length[v.piecePosition.lane.endLaneIndex])/2
                    delta = dat.dist + arclen - dat.olddist
                 else
                    local cross_over_len = dat.oldtrack.length * 1
                    local lane_dist = math.abs(self.lanes[dat.last_lane_end] - self.lanes[dat.last_lane_start])
                    delta =  (dat.oldtrack.length - cross_over_len) + math.sqrt(cross_over_len*cross_over_len + lane_dist*lane_dist) - dat.olddist + dat.dist
                 end
              else
                 delta = dat.dist + (dat.oldtrack.angle and dat.oldtrack.arc_length[dat.lane] or dat.oldtrack.length) - dat.olddist
              end
           end
           dat.totaldist = dat.totaldist + delta
           table.insert(dat.pos, dat.totaldist)
           
           if #dat.pos >= 4 then
            table.remove(dat.pos, 1)
           end

           dat.oldaccel = dat.accel or 0

           if #dat.pos > 1 then
              dat.oldspeed = dat.speed or 0
              dat.speed = (dat.pos[#dat.pos] - dat.pos[#dat.pos - 1])/TICKTIME
           else
              dat.speed = 0
           end

           if #dat.pos > 2 then
              local oldspeed = (dat.pos[#dat.pos-1] - dat.pos[#dat.pos - 2])/TICKTIME
              dat.accel = (dat.speed - oldspeed)/TICKTIME
           else
              dat.accel = 0
           end

        end


        --look ahead to find upcoming curves
        do
           local track_length = dat.track.length or dat.track.arc_length[dat.lane]
           local old_dist_to_end_of_curve = dat.dist_to_end_of_curve or 0
           dat.dist_to_curve = 0
        
           if dat.track.angle then
              dat.dist_to_curve = dat.dist_to_curve + dat.track.arc_length[dat.lane] - dat.dist
              dat.dist_to_end_of_curve = dat.dist_to_curve

              local track = dat.track.next_track
              while track and track.angle and math.sign(track.angle or 0) == math.sign(dat.track.angle) do
                 dat.dist_to_end_of_curve = dat.dist_to_end_of_curve + track.arc_length[dat.lane]
                 track = track.next_track
              end

              -- New turn means calculate the size of it.
              if dat.dist_to_end_of_curve > old_dist_to_end_of_curve then
                dat.curve_length = dat.dist_to_end_of_curve
              end

           else
              dat.dist_to_curve = dat.dist_to_curve + dat.track.length - dat.dist
              dat.dist_to_end_of_curve = 0
              dat.curve_length = 0
           end

           local track = dat.track.next_track

           local sign = math.sign(dat.track.angle or 0)
           -- TODO: test without the equals in the next segment
           while track.length or (track.radius == dat.track.radius and sign == math.sign(track.angle or 0)) do
              if track.length then sign = 0 end
              dat.dist_to_curve = dat.dist_to_curve + track.arc_length[dat.lane]
              track = track.next_track
              dat.upcoming_turn_index = track.id
           end

           -- More Learning Stuff
           -- Loop through all the next curves segments and find us a force modifier
           local next_curve_track = self.track[dat.upcoming_turn_index]
            
           local biggest_fm = 0
           local smallest_fm = 0
           if next_curve_track then
             sign = math.sign(next_curve_track.angle or 0)

             -- force modifiers can be positive or negative. We want the biggest (for faster curves) unless there's a negative one
             while sign == math.sign(next_curve_track.angle or 0) do
                local fm = next_curve_track.force_modifier

                if fm > 0 and fm > biggest_fm then
                  biggest_fm = biggest_fm + fm
                elseif fm < smallest_fm then
                  smallest_fm = smallest_fm + fm
                end
               next_curve_track = next_curve_track.next_track
             end
           end

          dat.upcoming_turn_force_modifier = biggest_fm   
          if smallest_fm < 0 then
            dat.upcoming_turn_force_modifier = smallest_fm
          end

           -- This should really be its own class but I'm out of time so....

           dat.upcoming_turn_radius = track.radius -- Just the first segment's radius
           dat.upcoming_turn_angle = track.angle
           dat.upcoming_turn_length = 0 --track.arc_length[dat.lane]

           while true do
              --dat.upcoming_turn_radius = math.min(dat.upcoming_turn_radius, track.radius)
              
              dat.upcoming_turn_length = dat.upcoming_turn_length + track.arc_length[dat.lane]
              if not track.next_track.angle or math.sign(track.next_track.angle) ~= math.sign(track.angle) or track.radius ~= track.next_track.radius then
                 break
              end
              track = track.next_track
           end
        end
     end
  end

  local dat = self.cardata[self.carname]


  -- [[ LEARNING ABOUT CARROTS IS FUN!
  -- A little more learning here. Reward the last track if it came well under its 60 degree allowance
  -- if a turn is over and the angle is low. raise the modifier
  if dat.oldtrack and dat.oldtrack ~= dat.track then
    if dat.oldtrack.angle and math.abs(dat.oldtrack.angle + (dat.track.angle or 0)) < 2 then
      self.messages = self.messages or ""
      if dat.angle < 20 then
        dat.oldtrack.force_modifier = dat.oldtrack.force_modifier + 3
        self.messages = self.messages .. "UPPING CURVE by: 3"
      elseif dat.angle < 30 then
        dat.oldtrack.force_modifier = dat.oldtrack.force_modifier + 2
        self.messages = self.messages .. "UPPING CURVE by: 2"
      elseif dat.angle < 40 then
        dat.oldtrack.force_modifier = dat.oldtrack.force_modifier + 1
        self.messages = self.messages .. "UPPING CURVE by: 1"
      elseif dat.angle < 50 then
        dat.oldtrack.force_modifier = dat.oldtrack.force_modifier + 0.5
        self.messages = self.messages .. "UPPING CURVE by: 0.5"
      end

    end

  end
  --]]

-- [[ LEARNING ABOUT STICKS IS FUN TOO!
  if need_schooling then
     self.messages = self.messages .. "  Learning... "

     -- Reduce the force modifier for this curve only
     if dat.track.force_modifier then
      -- TODO: Make this number dynamic: ie. crash at the start of a curve is more serious
      -- than at the end and needs a bigger adjustment
       dat.track.force_modifier = dat.track.force_modifier - 1
       self.messages = self.messages .. string.format(" Track: %d track.force_modifier: %.1f ", dat.track.id, dat.track.force_modifier or 0)
     end

     -- Separately If we crash twice Globally reduce the force constant across the whole system 
     -- REMEMBER: punish a failure more than you reward or the system falls apart.  
     if crashes >= 2 then
       force_multiplier = force_multiplier - 1
       self.messages = self.messages .. string.format(" Global force_multiplier: %.1f ", force_multiplier)
     end

     need_schooling = false -- We don't NEED NO THOUGHT CONTROL!!!!
  end
--]]



--[[  
  --this block will print out curves for excel!
  self:goAtSpeed(300)
  self:send(self:throttle(self.thr or 1))
  local rad = dat.track.radius and (dat.track.radius + (dat.track.angle < 0 and 1 or -1)*self.lanes[dat.lane]) or 0

  self:printStatus()

--]]

-- [[
   --do we want to switch lanes?
   local switching = self:doLaneSwitch()

   if not switching then
      self.controlString = ""
      --self:doNoSlip()
      
      self:updateThrottle()

      --if self.thr >= 1 and dat.track.length and self.turboAvailable and dat.dist_to_curve / dat.speed >= self.turboTime then
      if self.turboAvailable and ( self.longest_straightaway == dat.track or (dat.track.next_track == self.longest_straightaway and self.thr >= 1)) then
        self:send(self:turbo())
        self.controlString = "Turbo"
      else
        self:send(self:throttle(self.thr or 1))
      end
   else
      self.controlString = "Switch"
   end
   self:printStatus()

    --]]


   -- Notice the values of rolling friction if we've got our foot off the accelerator
   -- Weight for time so we don't get major upsets here
   if self.thr == 0 and dat.accel < 0  and dat.oldaccel < 0 and dat.angle < 2 and not dat.track.angle then
     new_decel_constant = math.abs(dat.accel) / dat.speed
     new_deccel_slope = dat.accel - dat.oldaccel
     lazyticks = lazyticks + 1
     if lazyticks == 1 then
      decel_constant = new_decel_constant
      deccel_slope = new_deccel_slope
     else
       decel_constant = decel_constant + ((new_decel_constant - decel_constant) / lazyticks )
       deccel_slope = deccel_slope + ((new_deccel_slope - deccel_slope) / lazyticks )
     end

     -- ok now update the decceleration slope data
     -- print (string.format("decel_constant %2.3f new_decel_constant:  %2.3f lazyticks %d deccel_slope %2.3f new_deccel_slope %2.3f", decel_constant, new_decel_constant, lazyticks, deccel_slope, new_deccel_slope))
   end

end

function NoobBot:onlapfinished(data)
   if data.car.name == self.carname then
      self.messages = (self.messages or "") .. string.format(" LAP %d TIME:  %2.3f ", data.lapTime.lap, data.lapTime.millis/1000)
      --assert(false)
   end
end

function NoobBot:oncrash(data)
   self.messages = (self.messages or "") .. string.format(" %s crashed. ", data.name)

   -- let's learn from our mistakes
   if data.name == self.carname then 
     crashes = crashes + 1
     need_schooling = true
   end

   self:send(self:ping())

   --assert(false)
end

function NoobBot:ongameend(data)
   self:send(self:ping())
   self:printStatus() -- Just to get the last lap time out of it.

   print("Final force_multiplier: ", force_multiplier)
   print("Number of crashes: ", crashes)

   print("Race ended")
end

function NoobBot:onerror(data)
   print("Error: " .. data)
   self:send(self:ping())
end

function NoobBot:msgloop()
   local line = self.conn:receive("*l")
   while line ~= nil do
      local msg = json.decode.decode(line)
      if msg then
    local msgtype = msg["msgType"]
    local fn = NoobBot["on" .. string.lower(msgtype)]
    if fn then
       fn(self, msg["data"])
    else
       if self.messages == nil then 
        print ("Got " .. msgtype )
       else 
         self.messages = self.messages .. " [ Got " .. (msgtype or "") .. "] "
       end
       self:send(self:ping())
    end
      end
      line = self.conn:receive("*l")
   end
   print ("received nil line")
end

if #arg == 4 then
   local host, port, name, key = unpack(arg)
   ready = true

   print("Connecting with parameters:")
   print(string.format("host=%s, port=%s, bot name=%s, key=%s", host, port, name, key ))

   local c = assert(socket.connect(host, port))
   bot = NoobBot.create(c, name, key )
   bot:run()
elseif #arg ==5 then
   local host, port, name, key, track = unpack(arg)
   ready = true

   print("Connecting with parameters:")
   print(string.format("host=%s, port=%s, bot name=%s, key=%s, track = %s", host, port, name, key, track or "" ))

   local c = assert(socket.connect(host, port))
   bot = NoobBot.create(c, name, key, track )
   bot:run()  
elseif #arg ==6 then
   local host, port, name, key, track, action = unpack(arg)
   ready = true

   local c = assert(socket.connect(host, port))
   bot = NoobBot.create(c, name, key, track )

   if action == "create" then
     print("Creating Race with parameters:")
     print(string.format("host=%s, port=%s, bot name=%s, key=%s, track = %s", host, port, name, key, track or "" ))
     bot:createRace()  
   else
     print("Joining Race with parameters:")
     print(string.format("host=%s, port=%s, bot name=%s, key=%s, track = %s", host, port, name, key, track or "" ))
     bot:joinTestRace()
   end

else
   print("Usage: ./run host port botname botkey")
end




